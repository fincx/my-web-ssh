# MyWebSSH

#### 介绍
MyWebSSH
由 SpringBoot + WebSocket + xterm 实现的远程ssh连接面板

只是完成了最基本功能，包括登录模块，校验模块等等都木有。先埋个坑，后续有时间在维护，感兴趣的小伙伴可以fork到本地，修改之后提pull Requests



实现成果：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/152819_9347e1c4_2353571.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0930/152950_47ecd769_2353571.png "屏幕截图.png")


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


